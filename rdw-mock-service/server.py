import json
from flask import Flask
from flask_restful import Resource, Api

app = Flask(__name__)
api = Api(app)

class Voertuigen(Resource):
    def get(self):
        with open('data.json', 'r') as f:
            return json.load(f)

api.add_resource(Voertuigen, '/voertuigen')

if __name__ == '__main__':
    app.run(debug=True) # This debug mode is not executed when in uwsgi mode
