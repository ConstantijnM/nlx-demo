#!/bin/sh

# Start server
echo "Starting server"
uwsgi --http :8000 --manage-script-name --mount /=server:app
