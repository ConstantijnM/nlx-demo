import re
import os
from flask import Flask, render_template, request, redirect, flash
from flask_wtf.csrf import CSRFProtect
import requests

app = Flask(__name__,
            static_url_path='',
            static_folder='static',)

app.config.update(
    WTF_CSRF_TIME_LIMIT=60*60*24
)

app.secret_key = os.environ['SECRET_KEY']

csrf = CSRFProtect(app)

@app.route('/', methods=['GET', 'POST'])
def index():
    if request.method == 'POST':
        burgerservicenummer = re.sub(
            '[^0-9]+', '', request.form['burgerservicenummer'])
        kenteken = re.sub('[^0-9a-zA-Z\-]+', '', request.form['kenteken'])

        headers = {
            'X-NLX-Request-Application-Id': 'Parkeervergunningapplicatie',
            'X-NLX-Request-Process-Id': 'Aanvragen van parkeervergunning',
            'X-NLX-Request-Subject-Identifier': burgerservicenummer,
            'X-NLX-Request-Data-Subject': 'kenteken={},burgerservicenummer={}'.format(kenteken, burgerservicenummer)
        }

        try:
            rdw_headers = {
                'X-NLX-Request-Data-Elements': 'kenteken,burgerservicenummer'
            }

            requests.get(os.environ['RDW_URL'], headers={
                         **headers, **rdw_headers}, timeout=2)

            brp_headers = {
                'X-NLX-Request-Data-Elements': 'burgerservicenummer,adres'
            }

            requests.get(os.environ['BRP_URL'], headers={
                         **headers, **brp_headers}, timeout=2)
            return redirect('/requested')
        except requests.exceptions.RequestException:
            flash(
                'Er is een fout opgetreden tijdens het verwerken van je verzoek. Probeer het later opnieuw.')

    return render_template('index.html', form=request.form)


@app.route('/requested')
def requested():
    return render_template('requested.html')


if __name__ == '__main__':
    app.run(debug=True)  # This debug mode is not executed when in uwsgi mode
