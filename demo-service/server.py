from flask import Flask
from flask_restful import Resource, Api, reqparse
from datetime import datetime

app = Flask(__name__)
api = Api(app)

class Echo(Resource):
    def get(self):
        parser = reqparse.RequestParser()
        parser.add_argument('X-NLX-Request-Organization', location='headers')
        parser.add_argument('X-NLX-Request-Logrecord-Id', location='headers')
        args = parser.parse_args()

        print(args)

        return {
            'message': 'Hi there, greetings from the nlx-demo API!',
            'local_time': datetime.now().isoformat(),
            'nlx_request_organization': args['X-NLX-Request-Organization'],
            'nlx_request_logrecord_id': args['X-NLX-Request-Logrecord-Id']
        }

api.add_resource(Echo, '/')

if __name__ == '__main__':
    app.run(debug=True) # This debug mode is not executed when in uwsgi mode
