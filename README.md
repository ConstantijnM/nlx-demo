NLX - Demo Code
===
[![Build Status](https://jenkins.nlx.io/job/nlx-demo/badge/icon?style=plastic)](https://jenkins.nlx.io/job/nlx-demo/)

*This repository holds dummy components in the context of __[NLX](https://github.com/VNG-Realisatie/nlx)__.*

**NLX** is an open source inter-organisational system facilitating federated authentication, secure connecting and protocolling in a large-scale, dynamic API landscape.

To keep the NLX repository clean, (dummy) API's and (dummy) client software is kept separately in this repository.


## Questions and contributions

Read more on how to ask questions, file bugs and contribute code and documentation in [`CONTRIBUTING.md`](CONTRIBUTING.md).

## Licence

Copyright © VNG Realisatie 2017  
[Licensed under the EUPL](LICENCE.md)
