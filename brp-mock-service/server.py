import json
from flask import Flask
from flask_restful import Resource, Api

app = Flask(__name__)
api = Api(app)

class IngeschrevenNatuurlijkePersonen(Resource):
    def get(self):
        with open('data.json', 'r') as f:
            return json.load(f)

api.add_resource(IngeschrevenNatuurlijkePersonen, '/ingeschreven_natuurlijke_personen')

if __name__ == '__main__':
    app.run(debug=True) # This debug mode is not executed when in uwsgi mode
