#!/bin/sh

# Start server
echo "Starting server"
uwsgi --https :8443,/tls/tls.crt,/tls/tls.key --manage-script-name --mount /=server:app
