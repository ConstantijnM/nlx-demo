FROM python:3.6-alpine3.7 AS build

# Install build tools.
RUN apk --no-cache add \
    gcc \
    linux-headers \
    musl-dev \
    pcre-dev \
    python3 \
    python3-dev \
    libressl-dev \
    zlib-dev && \
  pip3 install virtualenv && \
  virtualenv /app/env

WORKDIR /app
COPY requirements.txt /app
RUN /app/env/bin/pip install -r requirements.txt

# Release on latest alpine image.
FROM python:3.6-alpine3.7

RUN apk --no-cache add \
      mailcap \
      musl \
      pcre \
      zlib

COPY --from=build /app/env /app/env
ENV PATH="/app/env/bin:${PATH}"

COPY . /app
WORKDIR /app

COPY ./docker/start.sh /start.sh
RUN chmod +x /start.sh

EXPOSE 8443
CMD ["/start.sh"]
